package app;

import java.util.Scanner;

public class lesson3 {
    public static void main(String[] args) {
        System.out.println("Введите Ваше имя:");
        Scanner input =  new Scanner(System.in);
        String name = input.nextLine();
        System.out.println("Введите Ваш возвраст:");
        int age = input.nextInt();
        System.out.println("Введите сумму кредита:");
        double sumCredit = input.nextDouble();
        String message = (!name.equals("Bob") && age >= 18 && sumCredit <= age*100) ? "Кредит выдан":"Кредит не выдан";
        System.out.println(message);
    }
}

